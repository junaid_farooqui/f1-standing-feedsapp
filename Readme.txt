Package Contain:

-Index.html - HTML Code 

-JS Files
--Angular.js - JS Library
--Scripts.js - Module, Controller and Custom Functions

-CSS Files
--bootstrap.min.css - CSS Library
--Style.css - Custome define CSS

-Img
--Background
--Logo
--Flags

Description:

This is One page small app using third party Ergas API Service to display Formula 1 season standing feeds. This app is showing a list of drivers. You can choose season through drop down menu and a list of that season's driver will be display according their's points standing. you can filter that list with search field. 

Year Range is define 2005 to 2016, you can increase it in getYearRange() function by setting var endYear = yourRequiredVal

Installation:
Unzip and double click on index.html

Required:
Internet Required.

